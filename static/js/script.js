$(".up").click(function () { 
    var current = $(this).closest('.card');
    var prev = current.prev('.card');
    current.insertBefore(prev);
});

$(".down").click(function () { 
    var current = $(this).closest('.card');
    var next = current.next('.card');
    current.insertAfter(next);
});

$("#changeTheme").click(function(){
    $("body").toggleClass("body-blue");
    $(".card").toggleClass("bg-blue");
    $(this).toggleClass("bg-blue");
    
    var text = $(this).text();
    $(this).text( text === "Red Theme" ? "Blue Theme" : "Red Theme");
});

