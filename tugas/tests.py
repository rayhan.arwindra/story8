from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options 
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time

class testAccordion(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()

        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        self.browser.get('http://localhost:8000')

    def tearDown(self):
        self.browser.close()

    def test_page_is_displayed(self):
        #User sees the title and content of page
        self.assertIn('Story8', self.browser.title)
        assert "Profile" in self.browser.page_source

    def test_card_content(self):
        #User sees the headers of the accordion elements
        assert "Current Activities" in self.browser.page_source
        assert "Volunteer/Organizational Experience" in self.browser.page_source
        assert "Achievements" in self.browser.page_source
        assert "Work Experience" in self.browser.page_source

    def test_card_move_down(self):
        #User presses the down button of the first accordion element
        first_card = self.browser.find_elements_by_css_selector(".down")
        first_card[0].send_keys(Keys.RETURN)

        #User sees that the second and first accordions have swapped
        second_card = self.browser.find_element_by_css_selector(".card")
        assert "Volunteer/Organizational Experience" in second_card.text
    
    def test_card_move_up(self):
        #User presses the up button of second accordion
        second_card = self.browser.find_element_by_name("up2")
        second_card.send_keys(Keys.RETURN)

        #User sees that the first and second accordions have swapped
        first_card = self.browser.find_element_by_css_selector(".card")
        assert "Volunteer/Organizational Experience" in first_card.text

    def test_change_theme(self):
        #User presses the theme button
        button = self.browser.find_element_by_name("themeBtn")
        button.send_keys(Keys.RETURN)

        #Wait for theme to change
        time.sleep(3)

        card = self.browser.find_element_by_class_name("card")
        body = self.browser.find_element_by_tag_name("body")

        #User sees that the body and accordion has changed color
        self.assertEquals(card.value_of_css_property("background-color"), "rgba(15, 76, 117, 1)")
        self.assertEquals(body.value_of_css_property("background-color"), "rgba(27, 38, 44, 1)")
# Create your tests here.
